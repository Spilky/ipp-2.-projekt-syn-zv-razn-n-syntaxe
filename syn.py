#!/usr/bin/python

#SYN:xspilk00

# Import knihoven
import sys
import os
import codecs
import re
import pprint

# Globální proměnné
isSetInput = False
isSetOutput = False
isSetFormat = False
isSetBr = False

inputFile = sys.stdin
outputFile = sys.stdout
formatFile = ""

inputStr = ""
formatStr = ""

# Vytiskne nápovědu a ukončí běh skriptu
def printHelp():
	print("NAPOVEDA:\n")
	print("VyuzivanE parametry:")
	print("\t--input=filename - zadany vstupni soubor")
	print("\t--output=filename - zadany vystupni soubor")
	print("\t--format=filename - zadany formatovaci soubor")
	print("\t--br - na kazdem konci radku bude znacka <br />")
	exit(0)

# Vytiskne chybovou hlášku a ukončí běh skriptu s příslušným návratovým kódem
def printErr(msg, code):
	if(msg == "BAD_PARAMS"):
		sys.stderr.write('Spatny format parametru skriptu nebo byla pouzita zakazana kombinace parametru.')
	if(msg == "INPUT_FILE"):
		sys.stderr.write('Neexistujici zadany vstupni soubor nebo chyba otevreni zadaneho vstupniho souboru pro cteni.')
	if(msg == "OUTPUT_FILE"):
		sys.stderr.write('Chyba pri pokusu o otevreni zadaneho vystupniho souboru pro zapis (napr. kvuli nedostatecnemu opravneni).')
	if(msg == "BAD_FORMAT"):
		sys.stderr.write('Spatny zadany format formatovaciho souboru')

	sys.stderr.write('\n')
	closeFiles()
	exit(code)

# Zavře všechny otevřené soubory
def closeFiles():
	if(isSetInput):
		inputFile.close()

	if(isSetOutput):
		outputFile.close()

	if(isSetFormat):
		formatFile.close()

# Zpracovává parametry, v případě chyby volá funkci printErr()
def solveParams():
	if(len(sys.argv) == 2):
		if(sys.argv[1] == "--help"):
			printHelp()

	for x in range(1, len(sys.argv)):
		if(sys.argv[x][0:8] == "--input="):
			global isSetInput
			global inputFile

			if(isSetInput):
				printErr("BAD_PARAMS", 1)

			isSetInput = True

			try:
				inputFile = codecs.open(sys.argv[x][8:], 'r', 'utf-8')
			except:
				printErr("INPUT_FILE", 2)

		elif(sys.argv[x][0:9] == "--output="):
			global isSetOutput
			global outputFile

			if(isSetOutput):
				printErr("BAD_PARAMS", 1)

			isSetOutput = True

			try:
				outputFile = codecs.open(sys.argv[x][9:], 'w', 'utf-8')
			except:
				printErr("OUTPUT_FILE", 3)

		elif(sys.argv[x][0:9] == "--format="):
			global isSetFormat
			global formatFile

			if(isSetFormat):
				printErr("BAD_PARAMS", 1)

			isSetFormat = True
			try:
				formatFile = codecs.open(sys.argv[x][9:], 'r', 'utf-8')
			except:
				formatStr = 0

		elif(sys.argv[x][0:4] == "--br"):
			global isSetBr

			if(isSetBr):
				printErr("BAD_PARAMS", 1)

			isSetBr = True

		else:
			printErr("BAD_PARAMS", 1)

# Analyzuje a zpracovává formátovací soubor. Udděluje regulární výraz od popisu značky.
# V případě chyby je volána funkce printErr()
def getFormatTable():
	global formatStr
	formatTable = []

	for line in formatStr.splitlines():
		tag = []
		split = line.split('\t', 1)
		try:
			tag.append(split[0].strip())
			tag.append(split[-1].strip())
		except:
			printErr("BAD_FORMAT", 4)
		formatTable.append(tag)
	return formatTable

# Zjišťuje pozici HTML značek na základě regulárního výrazu.
# Příslušně značky jsou vkládány na určené indexy pole positions[]
def getTagPos(formatTable):
	global inputStr
	positions = [''] * (len(inputStr) + 1)
	for line in formatTable:
		pytReg = translateRegex(line[0])
		for find in re.finditer(pytReg, inputStr, re.DOTALL):
			if(find.end() != find.start()):
				positions[find.start()] = positions[find.start()] + getHtmlTags(line[1], True)
				positions[find.end()] = getHtmlTags(line[1], False) + positions[find.end()]
	return positions

# Převádí regulární výraz skriptu na regulární výraz srozumitelný pro Python 3
# V případě chyyby v regulárním výrazu je volána funkce printErr()
def translateRegex(regex):
	negation = ''
	pytReg = ''
	cont = False

	for i in range(0, len(regex)):
		if(regex[i] == '%' or cont):
			if(regex[i] == '%' and i == len(regex)-1 and regex[i] != '%'):
				printErr("BAD_FORMAT", 4)
			if(cont == False):
				cont = True
				continue

			if(regex[i] == 's'):
				pytReg += '[' + negation + ' \t\n\r\f\v]'
			elif(regex[i] == 'a'):
				if(negation == '^'):
					pytReg += '[[^A-Z]|[A-Z]]'
				else:
					pytReg += '.'
			elif(regex[i] == 'd'):
				pytReg += '[' + negation + '0-9]'
			elif(regex[i] == 'l'):
				pytReg += '[' + negation + 'a-z]'
			elif(regex[i] == 'L'):
				pytReg += '[' + negation + 'A-Z]'
			elif(regex[i] == 'w'):
				pytReg += '[' + negation + 'a-zA-Z]'
			elif(regex[i] == 'W'):
				pytReg += '[' + negation + '0-9a-zA-Z]'
			elif(regex[i] == 't'):
				pytReg += '[' + negation + '\t]'
			elif(regex[i] == 'n'):
				pytReg += '[' + negation + '\n]'
			elif(regex[i] in '.|!*+()%'):
				pytReg += negation + '\\' + regex[i]
			else:
				printErr("BAD_FORMAT", 4)

			negation = ''
			cont = False


		elif(regex[i] == '!'):
			if(negation == '^'):
				printErr("BAD_FORMAT", 4)
			negation = '^'
		elif(regex[i] == '.'):
			if(regex[i-1] == '.' or i == 0 or i == len(regex)-1):
				printErr("BAD_FORMAT", 4)
			continue
		else:
			if regex[i] in '\\^[]{}$?':
				pytReg += '\\'
			if negation == '^':
				pytReg += '[^'+ regex[i] +']'
				negation = ''
			else:
				if(regex[i] == ')' and regex[i-1] == '('):
					printErr("BAD_FORMAT", 4)
				if((regex[i] == '|' and regex[i-1] == '|') or (regex[i] == '|' and i == 0) or (regex[i] == '|' and i == len(regex)-1)):
					printErr("BAD_FORMAT", 4)
				pytReg += regex[i]

	try:
		re.compile('^.')
	except:
		printErr("BAD_FORMAT", 4)

	return pytReg

# Kontrola správnosti formátovacího souboru
def checkFormat(formatTable):
	for line in formatTable:
		getHtmlTags(line[1], True)
	return formatTable

# Zjištění HTML značky na základě řetězce, popisující tuto značku
# V případě nepodporovaného popisovače je volána funkce printErr()
def getHtmlTags(formatLine, start):
	htmlTags = ''
	for format in formatLine.split(','):
		format = format.strip()
		tag = '<'

		if(start == False):
			tag += '/'

		if(format == 'bold'):
			tag += 'b'
		elif(format == 'italic'):
			tag += 'i'
		elif(format == 'underline'):
			tag += 'u'
		elif(format == 'teletype'):
			tag += 'tt'
		elif(re.match('size:[1-7]', format)):
			tag += 'font'
			if(start):
				tag += ' size=' + format[5:]
		elif(re.match('color:[0-9a-fA-F]{6}', format)):
			tag += 'font'
			if(start):
				tag += ' color=#' + format[6:]
		else:
			printErr("BAD_FORMAT", 4)
		tag += '>'

		if(start):
			htmlTags += tag
		else:
			htmlTags = tag + htmlTags

	return htmlTags

# Generuje výstup, vkládáním HTML značek z pole positions[] na určené pozice
def generateOutput(positions):
	global inputStr
	finalString = ''
	for i in range(len(inputStr)):
		finalString += positions[i] + inputStr[i]
	finalString += positions[len(inputStr)]
	return finalString

# Hlavní tělo programu
solveParams()

inputStr = inputFile.read()

if(isSetFormat):
	if(formatStr == 0):
		outputStr = inputStr
		isSetFormat = False
	else:
		formatStr = formatFile.read()
		if(len(formatStr) == 0):
			outputStr = inputStr
			isSetFormat = False

else:
	outputStr = inputStr

if(isSetFormat):
	formatTable = checkFormat(getFormatTable())
	positions = getTagPos(formatTable)
	outputStr = generateOutput(positions)


if(isSetBr):
	outputStr = outputStr.replace("\n", "<br />\n")

outputFile.write(outputStr)

closeFiles()