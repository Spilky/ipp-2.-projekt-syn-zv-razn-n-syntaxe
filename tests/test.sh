cp ../syn.py ./syn.py
rm ./myoutput/LOG
./_stud_tests.sh


for x in 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22
do
	echo "---------------------------------------------------"
	echo "--------------------- TEST $x ---------------------"
	echo "---------------------------------------------------"
	printf "\n"
	echo "-------------------- OUTPUT -----------------------"
	printf "\n--------------------- TEST $x ---------------------\n" >> ./myoutput/LOG
	diff ./test${x}.out ./myoutput/test${x}.out 2>> ./myoutput/LOG >> ./myoutput/LOG
	if [ $? == 0 ]; then
		echo -e "\e[32mOK"
		echo -e "\e[0m"
	else
		echo -e "\e[31mERR"
		echo -e "\e[0m"
	fi
	echo "------------------- RETURN CODE -------------------"
	printf "\n---------------------------------------------------\n" >> ./myoutput/LOG
	diff ./test${x}.!!! ./myoutput/test${x}.!!! 2>> ./myoutput/LOG >> ./myoutput/LOG
	if [ $? == 0 ]; then
		echo -e "\e[32mOK"
		echo -e "\e[0m"
	else
		echo -e "\e[31mERR"
		echo -e "\e[0m"
	fi
	echo "---------------------------------------------------"
	printf "\n\n\n"
done
